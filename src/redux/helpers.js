const mapStateToProps = ({ cart: { cartItems } }) => ({
  items: cartItems,
  total: cartItems.reduce((acc, item) => {
    if (item.quantity > 2 && item.id === 3) {
      let _trunc = Math.trunc(item.quantity / 3);
      let differ = item.quantity - _trunc;
      acc += item.price * differ + item.discount * _trunc;
    } else {
      acc += item.price * item.quantity;
    }
    return acc;
  }, 0),
});

export default mapStateToProps;
