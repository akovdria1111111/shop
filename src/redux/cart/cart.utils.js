export const addItemToCart = (cartItems, cartItemToAdd) => {
  const existingCartItem = cartItems.find(
    (cartItem) => cartItem.id === cartItemToAdd.id
  );

  if (existingCartItem) {
    return cartItems.map((cartItem) =>
      cartItem.id === cartItemToAdd.id
        ? { ...cartItem, quantity: cartItem.quantity + 1 }
        : cartItem
    );
  }

  return [...cartItems, { ...cartItemToAdd, quantity: 1 }];
};

export const removeItemFromCart = (cartItems, id) =>
  cartItems.filter((item) => item.id !== id);

export const minusItemToCart = (cartItems, cartItemMinus) => {
  const existingCartItem = cartItems.find(
    (cartItem) => cartItem.id === cartItemMinus.id
  );

  if (existingCartItem.quantity === 1) {
    return cartItems.filter((cartItem) => cartItem.id !== cartItemMinus.id);
  }

  return cartItems.map((cartItem) =>
    cartItem.id === cartItemMinus.id
      ? { ...cartItem, quantity: cartItem.quantity - 1 }
      : cartItem
  );
};
