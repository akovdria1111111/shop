const SHOP_DATA = {
  fruit: {
    id: 1,
    title: "Fruit",
    routeName: "fruit",
    items: [
      {
        id: 1,
        name: "Bananas",
        image: "banana.jpg",
        price: 10,
      },
      {
        id: 2,
        name: "Apples",
        image: "apples.jpg",
        price: 8,
      },
      {
        id: 3,
        name: "Papaya",
        image: "papaya.jpg",
        price: 10,
        discount: 5,
      },
    ],
  },
};

export default SHOP_DATA;
