const brands = [
  {
    title: "Fruit",
    image: "brand-1.jpg",
    id: 1,
    link: "shop/fruit",
  },
];

export { brands };
