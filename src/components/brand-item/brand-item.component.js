import React from "react";
import { Link } from "react-router-dom";

import "./brand-item.styles.scss";

const BrandItem = ({ title, link }) => (
  <div className="brand-item">
    <Link to={link} className="brand-item__image-wrapper">
      <div
        className="brand-item__image-bg"
        style={{
          backgroundImage: `url(/images/fruit.jpg)`,
        }}
      ></div>
    </Link>
    <div className="brand-item__info">
      <Link to={link} className="brand-item__title">
        {title}
      </Link>
    </div>
  </div>
);

export default BrandItem;
