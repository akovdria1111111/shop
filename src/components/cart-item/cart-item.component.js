import React from "react";
import { connect } from "react-redux";
import {
  addItem,
  minusItem,
  removeItemById,
} from "../../redux/cart/cart.action";

import { ReactComponent as DeleteIcon } from "../../assets/icons/close.svg";

import "./cart-item.styles.scss";

const CartItem = ({ item, counter, removeItem, addItem, minusItem }) => {
  const { image, price, quantity, name, total } = item;
  const countItem = (acc, item) => {
    if (item.quantity > 2 && item.id === 3) {
      let _trunc = Math.trunc(item.quantity / 3);
      let differ = item.quantity - _trunc;
      acc += item.price * differ + item.discount * _trunc;
    } else {
      acc += item.price * item.quantity;
    }
    return acc;
  };
  return (
    <div className="cart-item">
      <div className="cart-item__image-column">
        <div className="cart-item__image-wrapper">
          <div
            className="cart-item__image-bg"
            style={{
              backgroundImage: `url(/images/${image})`,
            }}
          ></div>
        </div>
      </div>
      <div className="cart-item__info-column">
        <h2 className="cart-item__title">{name}</h2>
        <div className="cart-item__price-group">
          <span className="cart-item__price">${price}.00</span>
        </div>

        <div className="cart-item__total">{total}</div>
        {counter ? (
          <div className="cart-item__counter">
            <button
              className="cart-item__counter-button"
              onClick={() => minusItem(item)}
            >
              -
            </button>
            <span className="cart-item__counter-value">{quantity}</span>
            <span
              className="cart-item__counter-button"
              onClick={() => addItem(item)}
            >
              +
            </span>{" "}
            <div className="cart-item__totalItem">
              ${countItem(0, item)}
              .00
            </div>
          </div>
        ) : null}
      </div>
      <span className="cart-item__delete-button">
        <DeleteIcon
          className="cart-item__delete-icon"
          onClick={() => removeItem(item.id)}
        />
      </span>
    </div>
  );
};

const mapDispatchToProps = (dispatch) => ({
  removeItem: (id) => dispatch(removeItemById(id)),
  addItem: (item) => dispatch(addItem(item)),
  minusItem: (item) => dispatch(minusItem(item)),
});

export default connect(null, mapDispatchToProps)(CartItem);
