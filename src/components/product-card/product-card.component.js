import React from "react";
import { connect } from "react-redux";

import Button from "../button/button.component";
import { addItem } from "../../redux/cart/cart.action";

import "./product-card.styles.scss";

const ProductCard = ({ item, addItem }) => {
  const { name, image, price } = item;

  return (
    <div className="product-card">
      <div className="product-card__info">
        <p className="product-card__title">{name}</p>
        <div>
          <span className="product-card__price-item">${price}</span>
        </div>
      </div>
      <div className="product-card__image-wrapper">
        <div
          className="product-card__image-bg"
          style={{
            backgroundImage: `url(/images/${image})`,
          }}
        ></div>
        <div className="product-card__button-wrapper">
          <Button onClick={() => addItem(item)}>+ Add to cart</Button>
        </div>
      </div>
    </div>
  );
};

const mapDispatchToProps = (dispatch) => ({
  addItem: (item) => dispatch(addItem(item)),
});

export default connect(null, mapDispatchToProps)(ProductCard);
