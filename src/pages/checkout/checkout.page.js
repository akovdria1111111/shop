import React from "react";

import { Link, withRouter } from "react-router-dom";

import { connect } from "react-redux";

import TopTitle from "../../components/top-title/top-title.component";
import CartItem from "../../components/cart-item/cart-item.component";
import Button from "../../components/button/button.component";
import mapStateToProps from "../../redux/helpers";

import "./checkout.styles.scss";

const CheckoutPage = ({ items, total }) => (
  <div className="checkout-page">
    <TopTitle value="Checkout" />
    <div className="checkout-page__body">
      <div className="checkout-page__list">
        {items.map((item) => (
          <CartItem key={item.id} item={item} counter />
        ))}
      </div>
      <div className="checkout-page__footer">
        <div className="checkout-page__total">
          <span className="checkout-page__total-title">Total:</span>
          <span className="checkout-page__total-value">${total}.00</span>
        </div>
        <div className="checkout-page__button-wrapper">
          <Link to="/">
            <Button>Submit</Button>
          </Link>
        </div>
      </div>
    </div>
  </div>
);

export default withRouter(connect(mapStateToProps)(CheckoutPage));
