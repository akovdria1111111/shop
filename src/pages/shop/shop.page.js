import React from "react";
import { Route } from "react-router-dom";

import ProductsPage from "../../pages/products/products.page";

import BrandsWrapper from "./wrappers/brands/brands.wrapper";

const ShopPage = ({ match }) => (
  <>
    <Route
      exact
      path={`${match.path}`}
      component={() => (
        <>
          <BrandsWrapper />
        </>
      )}
    />
    <Route path={`${match.path}/:brandId`} component={ProductsPage} />
  </>
);

export default ShopPage;
