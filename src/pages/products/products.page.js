import React from "react";
import SHOP_DATA from "../../data/fruit.data";

import TopTitle from "../../components/top-title/top-title.component";

import ProductsWrapper from "./wrappers/products/products.wrapper";

const ProductsPage = ({
  match: {
    params: { brandId },
  },
}) => (
  <>
    <TopTitle value={`${SHOP_DATA[brandId.toString()].title}`} />
    <ProductsWrapper items={SHOP_DATA[brandId.toString()].items} />
  </>
);

export default ProductsPage;
